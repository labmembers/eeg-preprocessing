# EEG Data Preprocessing Workshop

Welcome to the EEG Data Preprocessing Workshop! This workshop covers:

- The neural origins and recording of electroencephalographic (EEG) signals
- Basic preprocessing steps for preparing EEG data for analyses
- How to perform EEG preprocessing steps in EEGLAB
- Widely-used analyis methods for processed EEG data

To access the workshop content go to the [repository wiki](https://bitbucket.org/labmembers/eeg-preprocessing/wiki/Home). 

Written by Daniel Feuerriegel, 5/18 at the University of Melbourne
